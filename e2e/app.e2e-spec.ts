import { ZaphomePage } from './app.po';

describe('zaphome App', () => {
  let page: ZaphomePage;

  beforeEach(() => {
    page = new ZaphomePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
