import {Component, OnInit} from '@angular/core';
import {ActuatorService} from './actuator/actuator.service';
import {Actuator} from './actuator/actuator.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  actuators: Actuator[] = [];

  constructor(private actuatorService: ActuatorService) {
  }

  ngOnInit() {
    this.actuatorService.findAllActuators().subscribe(
      res => this.onSuccess(res.json()),
      err => console.log('Could not load actuators: ' + err)
    );
  }

  private onSuccess(data) {
    for (let i = 0; i < data.actuators.length; i++) {
      this.actuators.push(data.actuators[i]);
    }
  }
}
