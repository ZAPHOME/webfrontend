import {Component, Input, OnInit} from '@angular/core';
import {Actuator} from './actuator.model';
import {ActuatorService} from './actuator.service';

@Component({
  selector: 'app-actuator',
  templateUrl: './actuator.component.html',
  styleUrls: ['./actuator.component.css']
})
export class ActuatorComponent implements OnInit {
  @Input() actuator: Actuator;

  constructor(private actuatorService: ActuatorService) {
  }

  ngOnInit() {
  }

  toggleState() {
    this.actuator.state.SwitchedOn = !this.actuator.state.SwitchedOn;
    this.actuatorService.switchActuator(this.actuator).subscribe(
      res => console.log('Switched actuator'),
      err => console.log('Error while trying to switch actuator: ' + err)
    );
  }

}
