import { State } from './state.model';

export class Actuator {
  constructor(public id: number,
              public name: string,
              public state: State) {
  }
}
