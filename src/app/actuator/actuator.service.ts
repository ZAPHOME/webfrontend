import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Actuator } from './actuator.model';

const API_URL = 'http://raspberrypi:8080/api/v1/';

@Injectable()
export class ActuatorService {
  constructor(private http: Http) {
  }

  findAllActuators(): Observable<any> {
    return this.http.get(API_URL + 'actuator');
  }

  switchActuator(actuator: Actuator): Observable<Response> {
    return this.http.post(API_URL + 'actuator/' + actuator.id + '/switch', actuator.state);
  }
}
