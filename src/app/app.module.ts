import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ActuatorComponent } from './actuator/actuator.component';
import { ActuatorService } from './actuator/actuator.service';
import { HttpModule } from '@angular/http';

@NgModule({
  declarations: [
    AppComponent,
    ActuatorComponent
  ],
  imports: [
    BrowserModule,
    HttpModule
  ],
  providers: [
    ActuatorService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
